#!/bin/bash

v=`emacs --version | head -1 | cut -f3 -d" " | cut -f1 -d"."`

if [ "$v" -lt "25" ]
then
    echo "emacs reports the following version, but we need at least 25"
    emacs --version
    exit 1
fi
mkdir -p ~/.emacs.d/elpa
cp -r dcoverage ~/.emacs.d/dcoverage

if [ -r "~/.emacs" ]
then
    echo "You already have an emacs config."
    echo "Copying it to ~/.emacs-real"
    cp ~/.emacs ~/.emacs-real
    echo "We are putting our config in ~/.emacs-651"
    echo "Please integrate what you need from it"
    cp config/emacs ~/.emacs-651
else
    cp config/.emacs ~/.emacs-real
fi

me=`whoami`
cp config/package-only ~/.emacs

#ask emacs to install our packages

echo "y" | emacs --batch -u $me --script config/pkg-install-script
emacs --batch -u $me --script config/meghanada 
#now put the config file in place
mv ~/.emacs-real ~/.emacs
