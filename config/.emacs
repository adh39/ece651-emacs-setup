(require 'package)
(add-to-list 'package-archives '("melpa" . "http://melpa.org/packages/"))
(package-initialize)

;; Uncomment this next line if you want line numbers on the left side
;(global-linum-mode 1)
(global-set-key "\C-c\C-v" 'compile)
(setq line-number-mode t)
(setq column-number-mode t)
(display-time)
(global-font-lock-mode t)
(setq font-lock-maximum-decoration t)

;;This makes rainbow delimiters mode the default.
;;comment out to turn it off.
(add-hook 'find-file-hook 'rainbow-delimiters-mode-enable)

;;Want electric pair mode? Uncomment the next line
;(electric-pair-mode)

;;Want to turn off show paren mode? Comment out the below line.
(show-paren-mode)
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(meghanada-mode-key-prefix [67108923] nil nil "Make it C-m instead of C-c")
 '(package-selected-packages
   (quote
    (realgud-jdb memoize meghanada lsp-javacomp lsp-java jtags javadoc-lookup java-imports gradle-mode flycheck-popup-tip flycheck-gradle cov company-rtags company-c-headers clang-format)))
 '(safe-local-variable-values (quote ((TeX-master . t)))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )


(global-set-key "\C-x\C-g" 'goto-line)


; Automatically set compilation mode to
; move to an error when you move the point over it
(add-hook 'compilation-mode-hook
 (lambda () 
   (progn
     (next-error-follow-minor-mode))))

;Automatically go to the first error
(setq compilation-auto-jump-to-first-error t)
(defun set-window-undedicated-p (window flag)
  "Never set window dedicated."
  flag)
 (setq-default indent-tabs-mode nil)
(advice-add 'set-window-dedicated-p :override #'set-window-undedicated-p)


(require 'company)
(require 'company-rtags)
(global-company-mode)
(require 'clang-format)


(add-hook 'c-mode-common-hook
          (function (lambda ()
                    (add-hook 'write-contents-functions
                              (lambda() (progn (clang-format-buffer) nil))))))

(add-hook 'cpp-mode-common-hook
          (function (lambda ()
                      (add-hook 'write-contents-functions
                                (lambda() (progn (clang-format-buffer) nil))))))
(add-to-list 'company-backends 'company-c-headers)
(require 'flycheck)
(global-flycheck-mode)
(flycheck-popup-tip-mode)

(defun colorize-grade-txt ()
  "Make a grade.txt file show colors, then set read only."
  (interactive)
  (if (or (string= (buffer-name) "grade.txt")
          (string-prefix-p "grade.txt<" (buffer-name)))
      (progn (let ((inhibit-read-only t))
               (ansi-color-apply-on-region (point-min) (point-max)))
             (set-buffer-modified-p nil)
             (read-only-mode t))))

(add-hook 'find-file-hook 'colorize-grade-txt)


(add-hook 'gud-mode-hook (lambda() (company-mode 0)))




(require 'gradle-mode)
(require 'meghanada)                    

(add-hook 'java-mode-hook
          (lambda()
            (meghanada-mode t)
            (flycheck-mode +1)
            (setq c-basic-offset 2)
            (gradle-mode)
            (local-set-key "\C-c\C-v" 'gradle-build)
            (local-set-key "\C-c\C-i" 'meghanada-import-all)
            ))


(require 'elquery)
(add-to-list 'load-path "~/.emacs.d/dcoverage/")
(require 'dcoverage)

(defun build-and-run ()
  "Single key stroke for gradle to build and run the program."
  (interactive)
  (gradle-run "--info build run"))

(define-key gradle-mode-map (kbd "C-c C-r") 'build-and-run)


