# ECE651 Emacs Setup

This repository hosts the basic Emacs setup for 651.

Short version: run ./setup.sh

This setup includes dcoverage.el, which will show coverage
information from Clover in Emacs.

Furthermore, the setup includes a script which will install
all of the recommended packages, so that you can just run it
and have the right emacs configuration.

Note that we assume you have at least Emacs 25
for use of this.  If you have a really old version of Emacs,
please upgrade first.


Further note that the Emacs configuration for this script
carries over a lot of 551's configuration (even though
much of the C configuration is not needed). 

Note that if you already have an Emacs configuration, 
we will install the packages, but leave your configuration
alone.   You can integrate the changes you need,


Once you have this installed, the key features for java you have are:

 * C-c TAB will add all needed imports to your code
 * C-c C-t will run your code and display test coverage using dcoverage.
 * C-C C-v will run gradle build (compile + run tests)
 * Plus all the good stuff you are used to from 551!
 
